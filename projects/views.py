from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "show_project": project,
    }
    return render(request, "projects/show_project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
    else:
        form = ProjectForm()
        # Create an instance of the Django
        # model form class
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
