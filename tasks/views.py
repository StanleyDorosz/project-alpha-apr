from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        # we should use the form to validate
        # the values and save them to
        # the database
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.owner = request.user
            task.save()
            return redirect("list_projects")
            # if all goes well, we can redirect
            # the browser to another page and
            # leave the function
    else:
        form = TaskForm()
        # Create an instance of the Django
        # model form class
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def show_my_tasks(request):
    list = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": list,
    }
    return render(request, "tasks/show_my_tasks.html", context)
